# README #

Hieronder mijn klassendiagram. Het lukte me niet om het als excel bestandje mee te pushen naar bitbucket.

Game

-element: HTMLElement
-car1: Car
-player1: Player
-player2: Player
-turn: number
-scoreboard: Scoreboard
-parkingSpot: ParkingSpot
-players: Player[]

+collision(): void
+draw(): void
+keyDownHandler(e : keyboardEvent)
+update()


GameItem

-element: HTMLElement
-name: string
-xPos: number
-yPos: number

+draw(HTMLElement)
+getHTMLElement()
+getElementId()
+update()

ParkingSpot (extends GameITem)


+draw()


Car  (extends GameITem)

-id: number

+move()
+getElemetId()
+remove()


Player

-name: string
-scoreboard: Scoreboard

+get name()


Scoreboard

-score: number

+get score()
+draw()
+update()
+addScore()

