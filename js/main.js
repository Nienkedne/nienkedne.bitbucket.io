class GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this.getElementId();
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    getHTMLElement() {
        return this._element;
    }
    getElementId() {
        return this._name;
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class Car extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    move(xPosition) {
        this._xPos -= xPosition;
    }
    getElementId() {
        return `${this._name}-${this._id}`;
    }
    remove(container) {
        var id = `${this._name}-${this._id}`;
        console.log(id);
        const elem = document.getElementById(id);
        console.log(elem);
        container.removeChild(elem);
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.keyDownHandler = (e) => {
            if (e.keyCode === 32) {
                this._car1.move(50);
                this.update();
            }
        };
        this._car1 = new Car('car1', 0);
        this._player1 = new Player('player1');
        this._player2 = new Player('player2');
        this._turn = 0;
        this._parkingSpot = new ParkingSpot('parkingSpot');
        this._scoreboard = new Scoreboard('scoreboard');
        this._players = [];
        this._players.push(this._player1);
        this._players.push(this._player2);
        window.addEventListener('keydown', this.keyDownHandler);
        this.draw();
    }
    collision() {
        const parkingSpotRect = document.getElementById('parkingSpot').getBoundingClientRect();
        const car1Rect = this._car1.getHTMLElement().getBoundingClientRect();
        if (car1Rect.right <= parkingSpotRect.right) {
            this._scoreboard.addScore();
            if (this._turn == 0)
                this._turn = 1;
            else {
                this._turn = 0;
            }
            document.getElementById('scoreboard').innerHTML = this._players[this._turn].name;
            console.log(this._car1);
            this._car1.remove(this._element);
            this._car1 = new Car('car' + (this._turn + 1), this._turn);
            this.draw();
        }
        else {
            console.log('no collision');
        }
    }
    draw() {
        this._car1.draw(this._element);
        this._parkingSpot.draw(this._element);
        this._scoreboard.draw(this._element);
    }
    update() {
        this._car1.update();
        this._parkingSpot.update();
        this._scoreboard.update();
        this.collision();
    }
}
const app = {};
(function () {
    let init = function () {
        app.game = new Game();
    };
    window.addEventListener('load', init);
})();
class ParkingSpot extends GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
}
class Player {
    constructor(name) {
        this._name = name;
    }
    get name() {
        return this._name;
    }
}
class Scoreboard extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    get score() {
        return this._score;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'The score is: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
    }
    addScore() {
        this._score += 1;
    }
}
//# sourceMappingURL=main.js.map