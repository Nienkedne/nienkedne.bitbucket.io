/// <reference path="gameItem.ts" />

class Car extends GameItem {
    //attributes
    private _id: number;

        /**
        * Function to create the car
        * @param {string} - name
        * @param {number} - id
        * @param {number} - xPosition
        * @param {number} - yPosition
        */
        constructor(name: string, id: number, xPosition: number = 0, yPosition: number = 0) {
            super(name, xPosition, yPosition);
            this._id = id;
        }

        /**
        * Function to move the car sidewards
        * @param {number} - xPosition
        */
        public move(xPosition: number): void {
            this._xPos -= xPosition;
            //this._element.classList.add
        }

        /**
        * Function to get the id of the name  
        */
        public getElementId(): string {
            return `${this._name}-${this._id}`;
        }
    
        /**
        * Function to remove the car 
        */
        public remove(container: HTMLElement) : void {
            var id = `${this._name}-${this._id}`;
            console.log(id);
            const elem = document.getElementById(id);
            console.log(elem);
            container.removeChild(elem);
        }
    
        
}