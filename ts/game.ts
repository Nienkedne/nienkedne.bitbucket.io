class Game {
  //attributes
  private _element: HTMLElement = document.getElementById('container');
  private _car1: Car;
  private _player1: Player;
  private _player2: Player;
  private _turn: number;
  private _scoreboard: Scoreboard;
  private _parkingSpot: ParkingSpot;
  private _players: Player[];

  /**
   * Create the Game class
   */
  constructor() {
    //create GameItems
    this._car1 = new Car('car1', 0);
    this._player1 = new Player('player1');
    this._player2 = new Player('player2');
    this._turn = 0;
    this._parkingSpot = new ParkingSpot('parkingSpot');
    this._scoreboard = new Scoreboard('scoreboard');
    this._players = [];
    this._players.push(this._player1);
    this._players.push(this._player2);

    //add keydown handler to the window object
    window.addEventListener('keydown', this.keyDownHandler);

    //draw initial state
    this.draw();

  }
  /**
   * Function to detect collision between two objects
   */
  public collision(): void {
    //use elem.getBoundingClientRect() for getting the right coordinates and measurements of the element
    const parkingSpotRect = document.getElementById('parkingSpot').getBoundingClientRect();
    const car1Rect = this._car1.getHTMLElement().getBoundingClientRect();


    if (car1Rect.right <= parkingSpotRect.right) {
      this._scoreboard.addScore();
      //change player
      if (this._turn == 0)
        this._turn = 1;
      else {
        this._turn = 0;
      }
      //change scoreboard to the other player
      document.getElementById('scoreboard').innerHTML = this._players[this._turn].name;
      // remove car from DOM
      console.log(this._car1);
      this._car1.remove(this._element);

      // put new car in DOM
      this._car1 = new Car('car' + (this._turn + 1), this._turn);
      this.draw();

    } else {
      console.log('no collision');
    }
  }

  //function to draw the initial state of all objects
  public draw(): void {
    this._car1.draw(this._element);
    this._parkingSpot.draw(this._element);
    this._scoreboard.draw(this._element);
  }

  /**
     * Function to handle the keyboard event
     * @param {KeyboardEvent} - event
     */
  public keyDownHandler = (e: KeyboardEvent): void => {
    if (e.keyCode === 32) {

      this._car1.move(50);
      this.update();
    }
  }

  /**
    * Function to update the state of all objects
    */
  public update(): void {
    this._car1.update();
    this._parkingSpot.update();
    this._scoreboard.update();
    this.collision();

  }

}
