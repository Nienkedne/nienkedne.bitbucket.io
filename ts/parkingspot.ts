/// <reference path="gameItem.ts" />

class ParkingSpot extends GameItem {


    /**
    * Function to create the parkingSpot
    * @param {string} - name
    * @param {number} - xPosition
    * @param {number} - yPosition
    */
    constructor(name: string, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition);

    }


    /**  Function to draw the initial state of the parkingSpot
    *  @param {HTMLElement} - container
    */
    public draw(container: HTMLElement): void {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;

        //create image
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;

        //append elements
        this._element.appendChild(image);
        container.appendChild(this._element);
    }

}

