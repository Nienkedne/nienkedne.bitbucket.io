class Player {
    //attributes
    private _name: string;
    private _scoreboard: Scoreboard;

    /**
        * Function to create the car
        * @param {string} - name
        */
    constructor(name: string) {
        this._name = name;
    }

    /**
     * Get the name
     */
    public get name(): string {
        return this._name;
    }

}